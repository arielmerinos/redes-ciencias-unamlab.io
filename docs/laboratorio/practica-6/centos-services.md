---
title: Configuración de servicios de red en el servidor CentOS
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Configuración de servicios de red en el **servidor** CentOS

## Diagrama de red

<a id="diagrama" name="diagrama"> </a>

| Diagrama de interfaces de red en VirtualBox
|:-----------------------------:|
| ![](img/diagrama_red.png)
| 🟠 <span class="orange">**Red DMZ**</span> - Conexiones que se manejan en la red **internal network** de VirtualBox
| 🔵 <span class="blue">**Red LAN**</span> - Conexiones que se manejan en la red **host-only** de VirtualBox
| 🟢 <span class="green">**Red WAN**</span> - Conexiones que se manejan en la red **NAT Network** de VirtualBox
| 🔴 <span class="red">**Red Externa**</span> - Conexiones que salen desde el equipo físico hacia Internet


## Instalar servicio Apache `httpd`

- Instalar los paquetes `httpd` y `mod_ssl`

```console
[root@centos ~]# dnf -y install httpd mod_ssl	⬅️
```

- Reiniciar el servicio `httpd`

```console
[root@centos ~]# systemctl restart httpd	⬅️
```

- Revisar que `mod_ssl` esté instalado y configurado

```console
[root@centos ~]# cat /etc/httpd/conf.modules.d/00-ssl.conf	⬅️
LoadModule ssl_module modules/mod_ssl.so	✅

[root@centos ~]# grep '^SSLCertificate*' /etc/httpd/conf.d/ssl.conf	⬅️
SSLCertificateFile /etc/pki/tls/certs/localhost.crt			✅
SSLCertificateKeyFile /etc/pki/tls/private/localhost.key	✅

[root@centos ~]# ls -la /etc/pki/tls/certs/localhost.crt /etc/pki/tls/private/localhost.key	⬅️
-rw-r--r--. 1 root root 3871 Nov  1 12:13 /etc/pki/tls/certs/localhost.crt		✅
-rw-------. 1 root root 1704 Nov  1 12:13 /etc/pki/tls/private/localhost.key	✅
```

- Revisar que `mod_ssl` esté cargado en Apache HTTPD

```
[root@centos ~]# apachectl -M | grep ssl	⬅️
AH00558: httpd:	...
 ssl_module (shared)	✅
```

!!! warning
    En algunos casos se muestra que el comando `apachectl` no soporta la opción `-M`

    `[root@centos ~]# apachectl -M | grep ssl	⬅️`
    `apachectl: The "-M" option is not supported.	❌`

    Se puede utilizar el comando `httpd -M` para obtener la información deseada

    `# httpd -M 2>/dev/null | grep ssl	⬅️`
    ` ssl_module (shared)	✅`

- Verificar que los puertos estén abiertos

```console
[root@centos ~]# netstat -ntulp | egrep -i '^Proto|httpd'	⬅️
Proto Recv-Q Send-Q Local Address    Foreign Address    State     PID/Program name
tcp6       0      0 :::80            :::*               LISTEN    1234/httpd	✅
tcp6       0      0 :::443           :::*               LISTEN    1234/httpd	✅
```

!!! note
    - En esta práctica no importa que el certificado sea [_auto-firmado_][wikipedia-self-signed-certificate] y que se muestre una advertencia de seguridad en los navegadores web.

## Configurar servicio Apache `httpd`

- Configurar que el servicio `httpd` esté activo y se inicie automáticamente con el sistema

```console
[root@centos ~]# systemctl enable --now httpd	⬅️
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.	✅

[root@centos ~]# systemctl status httpd	⬅️
	...
```

- Crea el archivo `index.html` en el directorio `/var/www/html` donde está la raíz del sitio web.

```console
[root@centos ~]# echo '<html><body><h1>It works</h1></body></html>' > /var/www/html/index.html
```

## Verificar el funcionamiento del servicio Apache `httpd`

- Verifica que puedas ver el archivo en `localhost` utilizando `curl`

- Conexión HTTP al puerto 80

```console
[root@centos ~]# curl -vk# 'http://localhost/'	⬅️
*   Trying ::1:80...
* Connected to localhost (::1) port 80 (#0)
> GET / HTTP/1.1
> Host: localhost
> User-Agent: curl/7.76.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK	✅
< Date: Fri, 11 Nov 2022 06:19:08 GMT
< Server: Apache/2.4.53 (CentOS Stream) OpenSSL/3.0.1
< Last-Modified: Fri, 11 Nov 2022 06:18:35 GMT
< ETag: "2d-5ed2bdc1cf211"
< Accept-Ranges: bytes
< Content-Length: 45
< Content-Type: text/html; charset=UTF-8
<
<html><body><h1>It works!</h1></body></html>	✅
* Connection #0 to host localhost left intact
```

- Conexión HTTPS al puerto 443 (no importa que el certificado sea [_auto-firmado_][wikipedia-self-signed-certificate])

```console
[root@centos ~]# curl -vk# 'https://localhost/'	⬅️
*   Trying ::1:443...
* Connected to localhost (::1) port 443 (#0)
* ALPN, offering h2
* ALPN, offering http/1.1
*  CAfile: /etc/pki/tls/certs/ca-bundle.crt
* TLSv1.0 (OUT), TLS header, Certificate Status (22):
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.2 (IN), TLS header, Certificate Status (22):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS header, Finished (20):
* TLSv1.2 (IN), TLS header, Unknown (23):
* TLSv1.3 (IN), TLS handshake, Encrypted Extensions (8):
* TLSv1.2 (IN), TLS header, Unknown (23):
* TLSv1.3 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS header, Unknown (23):
* TLSv1.3 (IN), TLS handshake, CERT verify (15):
* TLSv1.2 (IN), TLS header, Unknown (23):
* TLSv1.3 (IN), TLS handshake, Finished (20):
* TLSv1.2 (OUT), TLS header, Finished (20):
* TLSv1.3 (OUT), TLS change cipher, Change cipher spec (1):
* TLSv1.2 (OUT), TLS header, Unknown (23):
* TLSv1.3 (OUT), TLS handshake, Finished (20):
* SSL connection using TLSv1.3 / TLS_AES_256_GCM_SHA384	✅
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: C=US; O=Unspecified; CN=centos.local; emailAddress=root@centos.local
*  start date: Nov 11 01:03:10 2022 GMT
*  expire date: Nov 11 01:03:10 2023 GMT
*  issuer: C=US; O=Unspecified; OU=ca-156703581440990179; CN=centos.local; emailAddress=root@centos.local
*  SSL certificate verify result: self-signed certificate in certificate chain (19), continuing anyway.
* TLSv1.2 (OUT), TLS header, Unknown (23):
> GET / HTTP/1.1
> Host: localhost
> User-Agent: curl/7.76.1
> Accept: */*
>
* TLSv1.2 (IN), TLS header, Unknown (23):
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
* TLSv1.2 (IN), TLS header, Unknown (23):
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
* old SSL session ID is stale, removing
* TLSv1.2 (IN), TLS header, Unknown (23):
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK	✅
< Date: Fri, 11 Nov 2022 06:19:18 GMT
< Server: Apache/2.4.53 (CentOS Stream) OpenSSL/3.0.1
< Last-Modified: Fri, 11 Nov 2022 06:18:35 GMT
< ETag: "2d-5ed2bdc1cf211"
< Accept-Ranges: bytes
< Content-Length: 45
< Content-Type: text/html; charset=UTF-8
<
<html><body><h1>It works!</h1></body></html>	✅
* Connection #0 to host localhost left intact
```

- Edita los archivos HTML que necesites en el directorio `/var/www/html`, estos serán visibles bajo la URL `http://172.16.1.10/`

--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando tengas configurados los servicios de red del servidor CentOS

--------------------------------------------------------------------------------

|             ⇦             |         ⇧          |               ⇨               |
|:--------------------------|:------------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba][arriba] | [Página siguiente][siguiente]

[anterior]: ../centos-network
[arriba]: ../#procedimiento
[siguiente]: ../pfsense-dhcp

[wikipedia-self-signed-certificate]: https://en.wikipedia.org/wiki/Self-signed_certificate
