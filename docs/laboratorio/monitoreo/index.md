---
title: Monitoreo de equipos en la nube
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Monitoreo de equipos en la nube

Se implementó un sistema de monitoreo para las máquinas virtuales de la [práctica 8](../../laboratorio/practica-8) y [práctica 9](../../laboratorio/practica-9) con el objeto de ayudar a los alumnos a verificar que los puertos de sus equipos estén configurados de manera correcta.

## Mapa de estado

Existe un mapa de estado que muestra si el equipo puede ser alcanzado por PING (ICMP) desde el equipo de monitoreo.

| Nagios ➔ Status map
|:---------------------------------------:|
| <https://nagios.tonejito.cf/map.png>
| ![](img/nagios-map.png)

!!! note
    Este mapa se presenta como una imagen PNG que se refresa cada minuto gracias a algunas cabeceras especiales de HTTP, no es necesario recargar la página manualmente.

## Acceso al sistema de monitoreo

También se proporcionó a los alumnos acceso a un archivo que contiene las credenciales de acceso al sistema de monitoreo Nagios que está disponible en la siguiente URL:

| Nagios
|:-------------------------:|
| <https://nagios.tonejito.cf/nagios4/>
| ![](img/nagios-main.png)

En este sistema se puede consultar el estado general de los equipos en la sección `Hosts` que está en el panel izquierdo.
La liga directa para este panel de monitoreo es la siguiente:

| Nagios ➔ Hosts
|:-------------------------:|
| <https://nagios.tonejito.cf/cgi-bin/nagios4/status.cgi?host=all>
| ![](img/nagios-hosts.png)

También se puede consultar el estado de cada servicio monitoreado en la sección `Services` que está en el panel izquierdo.
La liga directa para este panel de monitoreo es la siguiente:

| Nagios ➔ Services
|:----------------------------:|
| <https://nagios.tonejito.cf/cgi-bin/nagios4/status.cgi?hostgroup=all&style=hostdetail>
| ![](img/nagios-services.png)

!!! note
    Las páginas se recargan automáticamente cada minuto, no es necesario refrescar la página manualmente.

--------------------------------------------------------------------------------
