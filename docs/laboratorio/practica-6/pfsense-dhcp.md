---
title: Configuración del servicio DHCP en pfSense
authors:
- Dante Erik Santiago Rodríguez Pérez
- Andrés Leonardo Hernández Bermúdez
---

# Configuración del servicio DHCP en pfSense

## Diagrama de red

<a id="diagrama" name="diagrama"> </a>

| Diagrama de interfaces de red en VirtualBox
|:-----------------------------:|
| ![](img/diagrama_red.png)
| 🟠  <span class="orange">**Red DMZ**</span> - Conexiones que se manejan en la red **internal network** de VirtualBox
| 🔵    <span class="blue">**Red LAN**</span> - Conexiones que se manejan en la red **host-only** de VirtualBox
| 🟢   <span class="green">**Red WAN**</span> - Conexiones que se manejan en la red **NAT Network** de VirtualBox
| 🔴 <span class="red">**Red Externa**</span> - Conexiones que salen desde el equipo físico hacia Internet

--------------------------------------------------------------------------------

Servicio DHCP de pfSense

- Dar clic en **Services** ⇨ **DHCP Server**  para entrar a la configuración del servicio local de DHCP

## DHCP en la interfaz <span class="orange">**OPT1**</span>

- Dar clic en la pestaña <span class="orange">**OPT1**</span> para configurar el servicio de DHCP en esa red

- Configurar el servicio con los siguientes parámetros:

| Elemento             | Valor
|:--------------------:|:--------:|
| Enable               | 🔲 **Deshabilitado**

!!! warning
    - La interfaz <span class="orange">**OPT1**</span> de la red <span class="orange">**DMZ**</span> **no requiere** el servicio de DHCP

## DHCP en la interfaz <span class="blue">**LAN**</span>

- Dar clic en la pestaña <span class="blue">**LAN**</span> para configurar el servicio de DHCP en esa red

- Configurar el servicio con los siguientes parámetros:

| Elemento             | Valor
|:--------------------:|:--------:|
| Enable               | ☑️ **Habilitado**
| Deny unknown clients | Allow all clients
| Subnet               | `192.168.42.0`
| Subnet mask          | `255.255.255.0`
| Range                | From `192.168.42.100` To: `192.168.42.199`
| Default lease time   | 300
| Maximum lease time   | 600

- Da clic en el botón **Save 💾** para guardar los ajustes

- Da clic en el botón **Apply Changes ✅** para aplicar los cambios

--------------------------------------------------------------------------------

### Reservar de direcciones DHCP en la <span class="blue">**red LAN**</span>

#### Identificar la dirección MAC del cliente en la <span class="blue">**red LAN**</span>

- Identificar la interfaz <span class="blue">**host-only**</span> de la máquina virtual Debian

| Interfaz <span class="blue">**host-only**</span> en la red <span class="blue">**LAN**</span>
|:--:|
| ![](img/debian/debian-settings-eth1-host-only.png)

- Identificar la dirección MAC del cliente Debian en la red <span class="blue">**LAN**</span> (host-only)

```console
tonejito@debian-11:~$ ip addr	⬅️
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc pfifo_fast state DOWN group default qlen 1000
    link/ether 08:00:27:ca:a3:8b brd ff:ff:ff:ff:ff:ff
    altname enp0s3
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:d9:06:ac brd ff:ff:ff:ff:ff:ff	🔆
    altname enp0s8
    inet 192.168.42.100/24 brd 192.168.42.255 scope global dynamic noprefixroute eth1	❗
       valid_lft 221sec preferred_lft 221sec
    inet6 fe80::a00:27ff:fed9:6ac/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

!!! warning
    - Verifica el tipo, nombre y conexión de las interfaces de red
    - Este ejemplo utiliza las interfaces de red `eth0` (<span class="gray">_desactivada_</span>) y `eth1` (host-only) para la <span class="blue">**red LAN**</span>
        - En algunos casos puede ser `enp0s3` (<span class="gray">_desactivada_</span>) y `enp0s8` (host-only) para la <span class="blue">**red LAN**</span>

#### Configurar la dirección IP reservada en el DHCP de pfSense

- Dar clic en **Services** ⇨ **DHCP Server**  para entrar a la configuración del servicio local de DHCP

- Dar clic en la pestaña <span class="blue">**LAN**</span> para configurar el servicio de DHCP en esa red

- Ir al final de la página en la sección **DHCP Static Mappings for this Interface**

- Da clic en el botón **Add ⤵️** para agregar una dirección IP reservada en DHCP

- Llenar el formulario con los siguientes parámetros:

| Elemento        | Valor | Notas
|:---------------:|:--------:|:--------:|
| MAC Address     | _Dirección MAC de la máquina virtual Debian_ | (ej. `08:00:27:d9:06:ac`)
| Client Identifier | LAN-Cliente-Debian-11 |
| IP Address | `192.168.42.10` | La dirección IP debe estar **FUERA** del rango asignado para DHCP
| Hostname | `debian-11`
| Description | Cliente Debian 11 en la red LAN
| Domain name | `lan`
| Default lease time | 600
| Maximum lease time | 900

- Da clic en el botón **Save 💾** para guardar los ajustes

- Da clic en el botón **Apply Changes ✅** para aplicar los cambios

--------------------------------------------------------------------------------

### Resumen de direcciones reservadas en DHCP

- Dar clic en **Services** ⇨ **DHCP Server** para entrar a la configuración del servicio local de DHCP

- Dar clic en la pestaña <span class="blue">**LAN**</span> para ver la configuración del servicio de DHCP en esa red

- Ir al final de la página en la sección **DHCP Static Mappings for this Interface**

| Direcciones reservadas en DHCP
|:----:|
| ![](img/pfsense/pfsense-58-webui-dhcp-reservations.png)

--------------------------------------------------------------------------------

### Estado del servicio DHCP

#### pfSense

- Dar clic en **Status** ⇨ **DHCP Leases** para visualizar las direcciones IP que ha entregado el servicio de DHCP a los clientes de la <span class="blue">**red LAN**</span>

- Verificar en pfSense que la configuración de DHCP tenga la dirección IP reservada

| Estado DHCP
|:----:|
| ![](img/pfsense/pfsense-59-webui-dhcp-leases.png)

#### Cliente en la <span class="blue">**red LAN**</span>

Verificar que el cliente tenga asignada la dirección IP reservada en DHCP

```console
tonejito@debian-11:~$ ip addr	⬅️
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc pfifo_fast state DOWN group default qlen 1000
    link/ether 08:00:27:ca:a3:8b brd ff:ff:ff:ff:ff:ff
    altname enp0s3
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:d9:06:ac brd ff:ff:ff:ff:ff:ff	🔆
    altname enp0s8
    inet 192.168.42.10/24 brd 192.168.42.255 scope global dynamic noprefixroute eth1	✅
       valid_lft 564sec preferred_lft 564sec
    inet6 fe80::a00:27ff:fed9:6ac/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

!!! warning
    - Verifica el tipo, nombre y conexión de las interfaces de red
    - Este ejemplo utiliza las interfaces de red `eth0` (<span class="gray">_desactivada_</span>) y `eth1` (host-only) para la <span class="blue">**red LAN**</span>
        - En algunos casos puede ser `enp0s3` (<span class="gray">_desactivada_</span>) y `enp0s8` (host-only) para la <span class="blue">**red LAN**</span>

!!! danger
    - Reiniciar la máquina virtual Debian si **NO** tiene la dirección IP que se reservó en DHCP

<!--

| Dirección MAC del Cliente Debian
|:----:|
| ![](img/pfsense/35.png)

- Ingresar al pfsense Services > DHCP Server > <span class="blue">**LAN**</span>
- En el apartado del final _DHCP Static Mappings for this Interface_, hacer clic en el botón _add_
- Llenar los siguientes parámetros:
    - MAC Address
    - IP Address
    - Hostname
    - Descripción: un nombre descriptivo
    - Seleccionar la casilla _Create an ARP Table Static_ para enlazar la IP y MAC
    - Al finalizar hacer clic en el botón _save_, posteriormente aplicar cambios con el botón _apply Changes_
-->
<!--
|DHCP Static Mapping parámetros
|:----:|
| ![](img/pfsense/37.png)

|DHCP Static Mapping Verificación
|:----:|
| ![](img/pfsense/38.png)

|Estado del servicio DHCP
|:----:|
| ![](img/pfsense/40.png)

|Validar dirección IP en virtual
|:----:|
| ![](img/pfsense/39.png)
-->

## Guardar configuraciíón final

Guardar la configuración en el archivo `pfsense-final.xml` e incluirlo en la carpeta `files/pfSense/`

| Exportar configuración a archivo XML
|:------------------------------------:|
| ![](img/pfsense/pfsense-039-webui-diagnostics-backup.png)
| ![](img/pfsense/pfsense-040-webui-backup-settings.png)
| ![](img/pfsense/pfsense-041-webui-backup-file.png)

--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando hayas terminado la configuración del servicio DHCP en pfSense

--------------------------------------------------------------------------------

|             ⇦             |         ⇧          |               ⇨               |
|:--------------------------|:------------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba][arriba] | [Página siguiente][siguiente]

[anterior]: ../centos-services
[arriba]: ../#procedimiento
[siguiente]: ../network-test
