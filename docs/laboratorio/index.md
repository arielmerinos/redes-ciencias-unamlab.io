---
title: Laboratorio
---

# Laboratorio

## Prácticas

| Título                                                                                  | Ambiente       | Entrega
|:----------------------------------------------------------------------------------------|:--------------:|:----------:|
| **Práctica 1**: [Laboratorio virtual de redes](practica-1) `¹`                          | VirtualBox     | 2023-02-21
| **Práctica 2**: [Configuración de _switches_ administrables](practica-2)                | Packet Tracer  | 2023-02-28
| **Práctica 3**: [Configuración de VLAN en _switches_ administrables](practica-3)        | Packet Tracer  | 2023-03-07
| **Práctica 4**: [Configuración de _routers_](practica-4)                                | Packet Tracer  | 2023-03-14
| **Práctica 5**: [Configuración de algoritmos de ruteo](practica-5)                      | Packet Tracer  | 2023-03-22
| **Práctica 6**: [Configuración de un _firewall_ de red con pfSense](practica-6)         | VirtualBox     | 2023-04-17 `²`
| **Práctica 7**: [Implementación de un servidor de archivos con NFS y Samba](practica-7) | VirtualBox     | 2023-04-24 `²`
| **Práctica 8**: [Registro de dominio DNS y creación de recursos en la nube](practica-8) | _Nube pública_ | 2023-05-04 `²`
| **Práctica 9**: [Implementación de sitios web sobre HTTPS](practica-9)                  | _Nube pública_ | 2023-05-11 `²`

!!! note
    - `¹`: Esta práctica debe ser entregada **individualmente**, todas las demás son **en equipo**

!!! warning
    - `²`: Se ajustó el periodo de elaboración de las actividades y las fechas de entrega para compensar los días en los que la Facultad de Ciencias estuvo en paro activo 🟥⬛

<!--
!!! warning
    - `⁰`: Fecha propuesta
    - `¹`: Se ajustó la fecha de entrega debido al sismo del día 19 de septiembre de 2022
    - `²`: Se ajustó el periodo de elaboración de las actividades y las fechas de entrega para compensar los días en los que la Facultad de Ciencias estuvo en paro activo
    - `³`: Se ajustó la fecha de entrega para dar espacio al segundo examen parcial
    - `⁴`: Se ajustó la fecha de entrega para tomar en cuenta los días de asueto
-->

--------------------------------------------------------------------------------

|                 ⇦            |        ⇧      |                  ⇨           |
|:-----------------------------|:-------------:|-----------------------------:|
| [Tareas][tareas]             | [Arriba](../) | [Proyectos][proyectos]       |

[arriba]: ../
[tareas]: ../tareas/
[laboratorio]: ../laboratorio/
[proyectos]: ../proyectos/
[examenes]: ../examenes/
