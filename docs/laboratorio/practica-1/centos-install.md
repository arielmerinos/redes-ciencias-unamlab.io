---
title: Instalación de CentOS Stream 9
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Instalación de CentOS Stream 9

!!! note
    - Si ya instalaste tu máquina virtual, continúa en [la siguiente página](../centos-configure)
    - No olvides verificar la configuración de VirtualBox

--------------------------------------------------------------------------------

## Descarga de imagen ISO

Descarga alguna de las imagenes ISO para instalar tu sistema operativo

- [Imagen ISO `boot` de CentOS Stream 9 💽][iso-boot-centos-stream-9]
- [Imagen ISO `boot` de Rocky Linux 9 💽][iso-boot-rocky-linux-9]
- [Imagen ISO `minimal` de Rocky Linux 9 💽][iso-minimal-rocky-linux-9]
- [Imagen ISO `boot` de Alma Linux 9 💽][iso-boot-alma-linux-9]
- [Imagen ISO `minimal` de Alma Linux 9 💽][iso-minimal-alma-linux-9]

<!--
|      |
|:----:|
| ![](img/centos-8/download/centos-001-homepage.png) |

|      |
|:----:|
| ![](img/centos-8/download/centos-002-stream-8-x86_64.png) |

|      |
|:----:|
| ![](img/centos-8/download/centos-003-select-mirror.png) |

|      |
|:----:|
| ![](img/centos-8/download/centos-004-download-iso.png) |

|      |
|:----:|
| ![](img/centos-8/download/centos-005-iso.png) |

|      |
|:----:|
| ![](img/centos-8/download/centos-006-downloaded.png) |
-->

--------------------------------------------------------------------------------

## Creación de máquina virtual

### Crear nueva máquina virtual

|      |
|:----:|
| ![](img/centos-8/virtualbox/centos-001-virtualbox-new-vm.png) |

<!--
|      |
|:----:|
| ![](img/centos-8/virtualbox/centos-001-virtualbox-new-vm-OK.png) |
-->

|      |
|:----:|
| ![](img/centos-8/virtualbox/centos-002-virtualbox-vm-name.png) |

|      |
|:----:|
| ![](img/centos-8/virtualbox/centos-003-virtualbox-vm-ram.png) |

|      |
|:----:|
| ![](img/centos-8/virtualbox/centos-004-virtualbox-vm-disk.png) |

|      |
|:----:|
| ![](img/centos-8/virtualbox/centos-005-virtualbox-vm-disk-type.png) |

|      |
|:----:|
| ![](img/centos-8/virtualbox/centos-006-virtualbox-vm-disk-allocation.png) |

|      |
|:----:|
| ![](img/centos-8/virtualbox/centos-007-virtualbox-vm-disk-size.png) |

### Agregar imagen ISO de instalación

### Iniciar máquina virtual

--------------------------------------------------------------------------------

## Instalación

|      |
|:----:|
| ![](img/centos-8/install/centos-001-install-boot-menu.png.png) |

### Seleccionar idioma

|      |
|:----:|
| ![](img/centos-8/install/centos-002-install-language.png) |

### Configurar contraseña de `root`

|      |
|:----:|
| ![](img/centos-8/install/centos-003-install-root-password-select.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-004-install-root-password.png) |

### Configuración de red

|      |
|:----:|
| ![](img/centos-8/install/centos-005-install-network-select.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-006-install-network-configuration-off.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-006-install-network-configuration.png) |

### Particionar disco

|      |
|:----:|
| ![](img/centos-8/install/centos-007-install-partition-select.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-008-install-partition-disk.png) |

### Seleccionar _software_ a instalar

|      |
|:----:|
| ![](img/centos-8/install/centos-009-install-software-select.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-010-install-software-select-server.png) |


### Iniciar instalación

|      |
|:----:|
| ![](img/centos-8/install/centos-011-install-begin.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-012-install-downloading.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-013-install-complete.png) |

### Reiniciar el equipo

|      |
|:----:|
| ![](img/centos-8/install/centos-014-install-reboot.png) |

### Aceptar la licencia de uso

|      |
|:----:|
| ![](img/centos-8/install/centos-015-install-license-select.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-016-install-license-accept.png) |

### Configuración de usuario y contraseña

|      |
|:----:|
| ![](img/centos-8/install/centos-017-install-user-select.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-018-install-user-create.png) |

### Finalizar configuración

|      |
|:----:|
| ![](img/centos-8/install/centos-019-install-finish-configuration.png) |

### Primer inicio de sesión

|      |
|:----:|
| ![](img/centos-8/install/centos-020-install-first-login-user.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-021-install-first-login-password.png) |

### Configuración inicial del usuario

|      |
|:----:|
| ![](img/centos-8/install/centos-022-install-first-login-welcome.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-023-install-first-login-keyboard.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-024-install-first-login-privacy.png) |

|      |
|:----:|
| ![](img/centos-8/install/centos-025-install-first-login-accounts.png) |

### Finalizar configuración inicial del usuario

|      |
|:----:|
| ![](img/centos-8/install/centos-026-install-first-login-complete.png) |

--------------------------------------------------------------------------------

!!! info
    - Cuando hayas terminado de instalar la máquina CentOS, [continúa con la configuración](../centos-configure)

[iso-boot-centos-stream-9]: https://mirror.rackspace.com/centos-stream/9-stream/BaseOS/x86_64/iso/CentOS-Stream-9-latest-x86_64-boot.iso
[iso-boot-rocky-linux-9]: https://download.rockylinux.org/pub/rocky/9/isos/x86_64/Rocky-9.1-x86_64-boot.iso
[iso-minimal-rocky-linux-9]: https://download.rockylinux.org/pub/rocky/9/isos/x86_64/Rocky-9.1-x86_64-minimal.iso

[iso-boot-alma-linux-9]: http://repo.almalinux.org/almalinux/9.1/isos/x86_64/AlmaLinux-9-latest-x86_64-boot.iso
[iso-minimal-alma-linux-9]: http://repo.almalinux.org/almalinux/9.1/isos/x86_64/AlmaLinux-9-latest-x86_64-minimal.iso
