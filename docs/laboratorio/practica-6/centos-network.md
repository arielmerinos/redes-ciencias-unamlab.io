---
title: Configuración de red del servidor CentOS
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Configuración de red del **servidor** CentOS

## Diagrama de red

<a id="diagrama" name="diagrama"> </a>

| Diagrama de interfaces de red en VirtualBox
|:-----------------------------:|
| ![](img/diagrama_red.png)
| 🟠 <span class="orange">**Red DMZ**</span> - Conexiones que se manejan en la red **internal network** de VirtualBox
| 🔵 <span class="blue">**Red LAN**</span> - Conexiones que se manejan en la red **host-only** de VirtualBox
| 🟢 <span class="green">**Red WAN**</span> - Conexiones que se manejan en la red **NAT Network** de VirtualBox
| 🔴 <span class="red">**Red Externa**</span> - Conexiones que salen desde el equipo físico hacia Internet

## Desconectar el adaptador de red NAT

- Ingresar a los ajustes de la máquina virtual en la sección de red
- Desmarcar la casilla 🔲 **Cable connected** para desconectar el cable de la red NAT de VirtualBox en el <u>primer</u> adaptador de red

| VirtualBox - Configuración red NAT
|:-----------------------------:|
| ![](img/centos/centos-settings-eth0-nat-disabled.png)

## Conectar el adaptador de red `intnet`

- Ingresar a los ajustes de la máquina virtual en la sección de red
- Dar clic en la pestaña **Adapter 2**
- Marcar la casilla ☑️ **Enable network adapter** para habilitar este adaptador de red
- Establecer el valor de **Attached to** en "internal network - `intnet`" para seleccionar una red interna de VirtualBox
- Establecer el valor de **Promiscuous Mode** como "Allow All"
- Marcar la casilla ☑️ **Cable connected** para conectar el cable de la red host-only de VirtualBox en el <u>segundo</u> adaptador de red

| VirtualBox - Configuración red `intnet`
|:-----------------------------:|
| ![](img/centos/centos-settings-eth1-intnet.png)

## Configurar dirección IP estática en Network Manager

Configurar una **dirección IP estática** en la <span class="orange">interfaz _host-only_</span> que está en la <span class="orange">red **DMZ**</span> (ver [diagrama de red](#diagrama))

!!! warning
    - Verifica el nombre de tu interfaz de red
    - Este ejemplo utiliza las interfaces de red `eth0` (NAT) y `eth1` (host-only) para la <span class="blue">**Red LAN**</span>
        - En algunos casos puede ser `enp0s3` (NAT) y `enp0s8` (host-only) para la <span class="blue">**Red LAN**</span>

- Ejecutar `nmtui` para editar las conexiones de red de Network Manager

```console
[root@centos ~]# nmtui
```

- Seleccionar **Activate a connection**

```text

                           ┌──┤ NetworkManager TUI ├──┐
                           │                          │
                           │  Please select an option │
                           │                          │
                           │  Edit a connection       │
                           │█ Activate a connection ██│	⬅️	①
                           │  Set system hostname     │
                           │                          │
                           │  Quit                    │
                           │                          │
                           │                     <OK> │
                           │                          │
                           └──────────────────────────┘

```

- Desactivar la conexión para la interfaz `eth1` (a veces aparece como `Wired network 2`)

    - Regresar al menú anterior con **Back**

```text

                     ┌─────────────────────────────────────┐
                     │                                     │
                     │ ┌────────────────────┐              │
                     │ │ Ethernet (eth0)  ↑ │ <Deactivate> │	⬅️	②	Deactivate
                     │ │   eth0           ▒ │              │
                     │ │                  ▒ │              │
                     │ │ Ethernet (eth1)  ▒ │              │
                     │ │ * eth1 █████████ ▒ │              │	⬅️	①
                     │ │                  ▒ │              │
                     │ │                  ▮ │              │
                     │ │                  ↓ │ <Back>       │
                     │ └────────────────────┘              │
                     │                                     │
                     └─────────────────────────────────────┘

```

- Seleccionar **Edit a connection**

```text

                           ┌──┤ NetworkManager TUI ├──┐
                           │                          │
                           │  Please select an option │
                           │                          │
                           │█ Edit a connection ██████│	⬅️	①
                           │  Activate a connection   │
                           │  Set system hostname     │
                           │                          │
                           │  Quit                    │
                           │                          │
                           │                     <OK> │
                           │                          │
                           └──────────────────────────┘

```

- Seleccionar `eth1` (o `Wired connection 2`)

    - Desmarcar la casilla **🔲 Automatically connect** para evitar que se conecte automáticamente al iniciar el sistema
    - Seleccionar **OK** para regresar al menú anterior

```text

   ┌───────────────────────────┤ Edit Connection ├───────────────────────────┐
   │                                                                         │
   │         Profile name eth1____________________________________           │
   │               Device eth1 (08:00:27:A5:A6:6E)________________           │
   │                                                                         │
   │ ═ ETHERNET                                                    <Show>    │
   │                                                                         │
   │ ═ IPv4 CONFIGURATION    <Automatic>                              <Show> │
   │ ═ IPv6 CONFIGURATION    <Automatic>                              <Show> │
   │                                                                         │
   │ [ ] Automatically connect █████████████████████████████████████████████ │	⬅️	①	🔲 Desmarcar
   │ [X] Available to all users                                              │
   │                                                                         │
   │                                                           <Cancel> <OK> │	⬅️	②	OK
   │                                                                         │
   └─────────────────────────────────────────────────────────────────────────┘

```

- Seleccionar **Add**

```text

                         ┌─────────────────────────────┐
                         │                             │
                         │ ┌───────────────┐           │
                         │ │ Ethernet    ↑ │ <Add>     │	⬅️	①	Add
                         │ │   eth0      ▒ │           │
                         │ │   eth1      ▒ │ <Edit...> │
                         │ │             ▒ │           │
                         │ │             ▒ │ <Delete>  │
                         │ │             ▮ │           │
                         │ │             ↓ │ <Back>    │
                         │ └───────────────┘           │
                         │                             │
                         └─────────────────────────────┘

```

- Seleccionar `Ethernet` y luego **Create**

```text

         ┌──────────────────────┤ New Connection ├──────────────────────┐
         │                                                              │
         │ Select the type of connection you wish to create.            │
         │                                                              │
         │                        DSL         ↑                         │
         │                     ██ Ethernet ██ ▮                         │	⬅️	①	Ethernet
         │                        InfiniBand  ▒                         │
         │                        Wi-Fi       ▒                         │
         │                        Bond        ↓                         │
         │                                                              │
         │                                            <Cancel> <Create> │	⬅️	②	Create
         │                                                              │
         └──────────────────────────────────────────────────────────────┘

```

- Establecer estos parámetros y después seleccionar **OK**

    - Nombrar la conexión como `eth1-dmz`
    - El dispositivo para la conexión es `eth1`
    - La dirección IP que tendrá el equipo CentOS es `172.16.1.10`
    - La dirección IP del equipo pfSense es `172.16.1.254` y funge como servidor DNS y _gateway_
    - Marcar la casilla ☑️ `Require IPv4 addressing for this connection` para forzar la conectividad por IPv4
    - Marcar la casilla ☑️ `Automatically connect` para permitir que se conecte automáticamente al iniciar el sistema

```text

   ┌───────────────────────────┤ Edit Connection ├───────────────────────────┐
   │                                                                         │
   │         Profile name eth1-dmz________________________________           │	⬅️
   │               Device eth1 (08:00:27:A5:A6:6E)________________           │	⬅️	①
   │                                                                         │
   │ ═ ETHERNET                                                    <Show>    │
   │                                                                         │
   │ ╤ IPv4 CONFIGURATION <Manual>                                 <Hide>    │
   │ │          Addresses 172.16.1.10/24___________ <Remove>                 │	⬅️	②
   │ │                    <Add...>                                           │
   │ │            Gateway 172.16.1.254_____________                          │	⬅️	③
   │ │        DNS servers 172.16.1.254_____________ <Remove>                 │	⬅️	④
   │ │                    <Add...>                                           │
   │ │     Search domains <Add...>                                           │
   │ │                                                                       │
   │ │            Routing (No custom routes) <Edit...>                       │
   │ │ [ ] Never use this network for default route                          │
   │ │ [ ] Ignore automatically obtained routes                              │
   │ │ [ ] Ignore automatically obtained DNS parameters                      │
   │ │                                                                       │
   │ │ [X] Require IPv4 addressing for this connection ██████████████████████│	⬅️	⑤	☑️ Marcar
   │ └                                                                       │
   │                                                                         │
   │ ═ IPv6 CONFIGURATION    <Automatic>                           <Show>    │
   │                                                                         │
   │ [X] Automatically connect                                               │	⬅️	⑥
   │ [X] Available to all users                                              │	⬅️	⑦
   │                                                                         │
   │                                                           <Cancel> <OK> │	⬅️	⑧	OK
   └─────────────────────────────────────────────────────────────────────────┘

```

- Seleccionar **Back** para regresar al menú anterior

```

                         ┌─────────────────────────────┐
                         │                             │
                         │ ┌───────────────┐           │
                         │ │ Ethernet    ↑ │ <Add>     │
                         │ │   eth0      ▒ │           │
                         │ │   eth1      ▒ │ <Edit...> │
                         │ │   eth1-dmz  ▒ │           │
                         │ │             ▒ │ <Delete>  │
                         │ │             ▮ │           │
                         │ │             ↓ │ <Back>    │	⬅️	①	Back
                         │ └───────────────┘           │
                         │                             │
                         └─────────────────────────────┘

```

- Seleccionar **Quit** para salir de `nmtui`

```

                           ┌──┤ NetworkManager TUI ├──┐
                           │                          │
                           │  Please select an option │
                           │                          │
                           │  Edit a connection       │
                           │  Activate a connection   │
                           │  Set system hostname     │
                           │                          │
                           │█ Quit ███████████████████│	⬅️	①	Quit
                           │                          │
                           │                     <OK> │
                           │                          │
                           └──────────────────────────┘

```

!!! note
    - **Opcionalmente** puedes activar la conexión `eth1-dmz` para probar que funcione

    `[root@centos ~]# nmcli connection up eth1-dmz	⬅️`

    `[root@centos ~]# nmcli connection show	⬅️`

    Esta conexión se activará automáticamente cuando se reinicie el equipo

### Reiniciar el equipo

- Reiniciar el equipo para verificar que la configuración de IP estática se realiza de manera automática

```console
[root@centos ~]# reboot	⬅️
```

### Verificar la configuración

- Verificar la configuración una vez que el equipo se haya reiniciado

```console
[root@centos ~]# nmcli connection show	⬅️
NAME      UUID                                  TYPE      DEVICE
eth1-dmz  3de1d762-337e-430f-b322-33adc0910b09  ethernet  eth1	✅
eth0      cda21fd0-6977-36bb-afce-9fecf21dc695  ethernet  --
eth1      2a51a8ce-619d-3da8-a93e-ed46c3ced639  ethernet  --

[root@centos ~]# hostname -I	⬅️
172.16.1.10

[root@centos ~]# ip route	⬅️
default via 172.16.1.254 dev eth1 proto static metric 100	✅
172.16.1.0/24 dev eth1 proto kernel scope link src 172.16.1.10 metric 100

[root@centos ~]# cat /etc/resolv.conf	⬅️
nameserver 172.16.1.254	✅

[root@centos ~]# ip addr	⬅️
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 08:00:27:eb:e3:a1 brd ff:ff:ff:ff:ff:ff
    altname enp0s3
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a5:a6:6e brd ff:ff:ff:ff:ff:ff	🔆
    altname enp0s8
    inet 172.16.1.10/24 brd 172.16.1.255 scope global noprefixroute eth1	✅
       valid_lft forever preferred_lft forever
    inet6 fe80::8ec5:5a15:6d74:e8c0/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

!!! warning
    - Verifica el tipo, nombre y conexión de las interfaces de red
    - Este ejemplo utiliza las interfaces de red `eth0` (<span class="gray">_desactivada_</span>) y `eth1` (intnet) para la <span class="blue">**red DMZ**</span>
        - En algunos casos puede ser `enp0s3` (<span class="gray">_desactivada_</span>) y `enp0s8` (intnet) para la <span class="blue">**red DMZ**</span>

## Reglas de `firewalld`

Deshabilitar el servicio de `firewalld` para quitar todas las reglas del _firewall_ local del equipo CentOS

```console
[root@centos ~]# systemctl disable --now firewalld	⬅️
```

<!--
Agregar las excepciones para los servicios de red que tiene el equipo

```console
[root@centos ~]# firewall-cmd --permanent --zone=public --add-service=ssh	⬅️
[root@centos ~]# firewall-cmd --permanent --zone=public --add-service=http	⬅️
[root@centos ~]# firewall-cmd --permanent --zone=public --add-service=https	⬅️
```

Recargar las reglas

```console
[root@centos ~]# firewall-cmd --reload	⬅️
```

Listar los servicios permitidos en `firewalld`

```console
[root@centos ~]# firewall-cmd --list-services	⬅️
cockpit dhcpv6-client http https ssh	✅

[root@centos ~]# firewall-cmd --list-all	⬅️
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: eth1
  sources:
  services: cockpit dhcpv6-client http https ssh	✅
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

- Habilitar el servicio de `firewalld`

```console
[root@centos ~]# systemctl enable --now firewalld	⬅️
```

!!! note
    Deshabilitar el servicio de `firewalld` si hay problemas de conectividad entre los equipos.

    ```
    [root@centos ~]# systemctl disable --now firewalld	⬅️
    ```
-->

--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando tengas configuradas las interfaces de red del servidor CentOS

--------------------------------------------------------------------------------

|             ⇦             |         ⇧          |               ⇨               |
|:--------------------------|:------------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba][arriba] | [Página siguiente][siguiente]

[anterior]: ../network-debian
[arriba]: ../#procedimiento
[siguiente]: ../centos-services
