---
title: Tareas
---

# Tareas

| Título                                                     | Ambiente      | Entrega
|:-----------------------------------------------------------|:-------------:|:----------:|
| **Tarea 1**: [Flujo de trabajo en GitLab](tarea-1)         | GitLab        | 2023-02-10
| **Tarea 2**: [Curso de nivelación de GNU/Linux](tarea-2)   | GNU/Linux     | 2023-02-13
| **Tarea 3**: [Instalación de Cisco Packet Tracer](tarea-3) | Packet Tracer | 2023-02-14
| <s>**Tarea 4**: Captura de tráfico de red `¹`</s>       | <s>WireShark</s> | <s>2023-03-28</s> `²`

!!! note
    - `¹`: Esta tarea debe ser entregada **en equipo**, todas las demás son **individuales**

!!! warning
    - `²`: Se **canceló** la entrega de esta actividad debido a que la Facultad de Ciencias entró en paro activo 🟥⬛
<!--
    `²`: Se ajustó el periodo de elaboración de las actividades y las fechas de entrega para compensar los días en los que la Facultad de Ciencias estuvo en paro activo.
-->

--------------------------------------------------------------------------------

|                 ⇦            |        ⇧      |                  ⇨           |
|:-----------------------------|:-------------:|-----------------------------:|
| [Examenes][examenes]         | [Arriba](../) | [Laboratorio][laboratorio]   |


[arriba]: ../
[tareas]: ../tareas/
[laboratorio]: ../laboratorio/
[proyectos]: ../proyectos/
[examenes]: ../examenes/
